/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34727
 */
public interface ClienteDAO {
    public List<Cliente> SelectAllCliente();
    public Boolean InsertCliente(String  dni_cliente,String nombre_cliente, String apellido_cliente,String ciudad);
    public boolean deleteClientes(String dni_cliente);
    public Boolean updateClientes(String dni_cliente,String nombre_cliente, String apellido_clientes,String ciudad);
    
     public ResultSet SelectClienteByName(String nombre_cliente);
}
