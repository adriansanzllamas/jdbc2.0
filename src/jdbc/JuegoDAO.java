/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34727
 */
public interface JuegoDAO {
    public List<Juego> SelectAllJuego();
    public Boolean InsertJuego(int codigo_juego,String nombre_juego, String creador,double precio, String fecha);
    public boolean deleteJuego(int codigo_juego);
    public Boolean updateJuego(int codigo_juego,String nombre_juego, String creador,double precio, String fecha);
    
     public ResultSet SelectJuegoByName(String nombre_juego);
}

