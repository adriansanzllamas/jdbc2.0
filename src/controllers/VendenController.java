/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Cliente;
import jdbc.Juego;
import jdbc.Venden;
import jdbc.VendenDAO;

/**
 *
 * @author 34727
 */
public class VendenController implements VendenDAO {
private final String SELECT_ALL = "select * from cliente";

private Connection con;
public VendenController(Connection con){
this.con = con;
}

    @Override
    public List<Venden> SelectAllVenden() {
            PreparedStatement stm;
ResultSet rs = null;
List <Venden> lista = new ArrayList<Venden>();
    try{
      stm = this.con.prepareStatement(SELECT_ALL);
          rs = stm.executeQuery();
   while (rs.next()) {

	String fecha = rs.getString("fecha_venta");
	int codigo = rs.getInt("codigo_juego");
	int empleado = rs.getInt("codigo_empleado");
	String cliente = rs.getString("dni_cliente");

	Venden c=new Venden(fecha, codigo, empleado, cliente);

	lista.add(c);
   }
stm.close();
rs.close();
} catch (SQLException e){
e.printStackTrace();
}
return lista;
    }

    @Override
    public Boolean InsertVenden(String fecha, int codigo_juego, int codigo_empleado, String dni_cliente) {
         String sql = "insert into venden ( fecha_venta,codigo_juego,codigo_empleado ,dni_cliente ) values (?,?,?,?)";
      
      try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1,fecha);
			ps.setInt(2, codigo_juego);
			ps.setInt(3,codigo_empleado);
			ps.setString(4,dni_cliente);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar en Venden");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public boolean deleteVenden(String dni_cliente) {
         String sql = "delete from venden where dni_cliente = ?";
    	try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, dni_cliente);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar la venta");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public Boolean updateVenden(String fecha, int codigo_juego, int codigo_empleado, String dni_cliente) {
   String sql = "insert into venden ( fecha_vente,codigo_juego,codigo_empleado ,dni_cliente ) values (?,?,?,?)";
      
      try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1,fecha);
			ps.setInt(2, codigo_juego);
			ps.setInt(3,codigo_empleado);
			ps.setString(4,dni_cliente);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el cliente");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public ResultSet SelectVendenByFecha(Date fecha) {
        throw new UnsupportedOperationException("Not supported yet."); //este no hace falta
    }

 

    
}
