/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34727
 */
public interface VendenDAO {
     public List<Venden> SelectAllVenden();
    public  Boolean InsertVenden(String fecha ,int codigo_juego,int codigo_empleado, String dni_cliente);
    public boolean deleteVenden(String dni_cliente);
    public Boolean updateVenden(String fecha ,int codigo_juego,int codigo_empleado, String dni_cliente);
    
     public ResultSet SelectVendenByFecha(Date fecha);
}
