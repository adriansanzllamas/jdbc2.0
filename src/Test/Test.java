/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;


import controllers.ClienteController;
import controllers.EmpleadoController;
import controllers.JuegoController;
import controllers.VendenController;
import java.sql.Connection;
import java.sql.Date;
import java.util.List;
import jdbc.Cliente;
import jdbc.Empleado;
import jdbc.Juego;
import jdbc.Venden;
import utils.Conexion;

/**
 *
 * @author 34727
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      //creamos la conexion....
      Conexion utilidades= new Conexion("jdbc:mysql://localhost:3307/tiendavideojuegos","root","1234");
      Connection conn=utilidades.getConnection();
      
      //probar cada metodo.
   ClienteController cr= new ClienteController(conn);
      
      List <Cliente> lista=cr.SelectAllCliente();
      for(Cliente c:lista){
          System.out.println(c.getNombre_cliente()+" "+c.getApellido_cliente()+" "+c.getDni_cliente()+" "+c.getCiudad());//ponemos todo para que te saque la informacion de la lista
      }
  
        System.out.println( cr.deleteClientes("43655669T"));
        System.out.println(cr.InsertCliente("43655669T", "jesus", "sanz", "madrid"));
        System.out.println(cr.updateClientes("43655669T", "pablo", "sanz", "Barcelona"));
        System.out.println(cr.SelectAllCliente());
        
 System.out.println("-------------------------------------------------------------------------------------------------------");
        
        EmpleadoController ec=new EmpleadoController(conn);
        
      List<Empleado> lista2 = ec.SelectAllEmpleado();
 
      for(Empleado v:lista2){
          System.out.println(v.getCodigo_empleado()+" "+v.getNombre_empleado()+" "+v.getApellido_empleado()+" "+v.getCiudad());
      }
        System.out.println(ec.InsertEmpleado(200, "Maria", "toscana", "Valencia"));
        System.out.println(ec.deleteEmpleado(200));
        System.out.println(ec.updateEmpleado(200, "Mariano", "Gutierrez", "Pais vasco"));
        
         System.out.println("-------------------------------------------------------------------------------------------------------");
         
         
         JuegoController jc=new JuegoController(conn);
         List <Juego> lista3=jc.SelectAllJuego();
         
         for(Juego t:lista3){
              System.out.println(t.getCodigo_juego()+" "+t.getNombre_juego()+" "+t.getCreador()+" "+t.getPrecio()+" "+t.getFecha_salida());
         }
         
         /*
         si funcionan pero como es tipo Date no me deja insertar la fechha pero cambiandolo por un String en susu clases si deja *ya esta cambiado
         */
         System.out.println(jc.InsertJuego(300, "nada", "nadie", 34, "2020-09-09")); // al ser tipo date no me deja inicializarlo pero pondria un String
         System.out.println(jc.deleteJuego(1184567891));
         System.out.println(jc.updateJuego(300, "kkkk", "jjjjj", 20, "2019-06-09"));//me pasa lo mismo con el tipo Dtae pero seruia cambiarlo y poner un String en vez de Date
         
         
         System.out.println("---------------------------------------------------------------------------------------------------------------------------------------");
       
         VendenController vc=new VendenController(conn);
         
         List<Venden> lista4=vc.SelectAllVenden();
         for(Venden w:lista4){
             System.out.println(w.getFecha_venta()+" "+w.getCodigo_empleado()+" "+w.getCodigo_juego()+" "+w.getDni_cliente());
         }
         System.out.println(vc.InsertVenden("2020-05-05", 30000, 6565867, "657465746T"));
         System.out.println(vc.deleteVenden("657465746T"));
         System.out.println(vc.updateVenden("2020-09-04", 30000, 362843678, "63846486t"));
  
          utilidades.closeConnection(conn);
    }
    
    
}
