package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbc.Cliente;
import jdbc.ClienteDAO;
import jdbc.Juego;


public class ClienteController implements ClienteDAO {
private final String SELECT_ALL = "select * from cliente";

private Connection con;
public ClienteController(Connection con){
this.con = con;
}

    @Override
    public boolean deleteClientes(String dni_cliente) {
    String sql = "delete from cliente where dni_cliente = ?";
    	try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, dni_cliente);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar el cliente");
			e.printStackTrace();
			return false;
		}

		return true;

 
    }

 

    @Override
    public Boolean InsertCliente(String dni_cliente, String nombre_cliente, String apellido_cliente, String ciudad) {
      String sql = "insert into cliente ( dni_cliente,nombre_cliente,apellido_cliente ,ciudad ) values (?,?,?,?)";
      
      try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1,dni_cliente);
			ps.setString(2, nombre_cliente);
			ps.setString(3, apellido_cliente);
			ps.setString(4,ciudad);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el cliente");
			e.printStackTrace();
			return false;
		}

		return true;


    }

    @Override
    public Boolean updateClientes(String dni_cliente, String nombre_cliente, String apellido_cliente, String ciudad) {
      String sql = "update cliente set  nombre_cliente = ?, apellido_cliente = ?, ciudad= ? where dni_cliente = ?";
      
      try {
			PreparedStatement ps = con.prepareStatement(sql);

			
			ps.setString(1, nombre_cliente);
			ps.setString(2, apellido_cliente);
                        ps.setString(3, ciudad);
                        ps.setString(4, dni_cliente);
			
			
			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al actualizar el cliente");
			e.printStackTrace();
			return false;
		}

		return true;


    }

    @Override
    public ResultSet SelectClienteByName(String nombre_cliente) {
        throw new UnsupportedOperationException("Not supported yet."); //Este no hace falta hacerlo.
    }

    @Override
    public List<Cliente> SelectAllCliente() {
        PreparedStatement stm;
ResultSet rs = null;
List <Cliente> lista = new ArrayList<Cliente>();
    try{
      stm = this.con.prepareStatement(SELECT_ALL);
          rs = stm.executeQuery();
   while (rs.next()) {

	String dni = rs.getString("dni_cliente");
	String nombre = rs.getString("nombre_cliente");
	String apellido = rs.getString("apellido_cliente");
	String ciudad = rs.getString("ciudad");

	Cliente c=new Cliente(dni, nombre, apellido, ciudad);

	lista.add(c);
   }
stm.close();
rs.close();
} catch (SQLException e){
e.printStackTrace();
}
return lista;
    }

   

   
  

   
}