/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Empleado;
import jdbc.Juego;
import jdbc.JuegoDAO;

/**
 *
 * @author SEGUNDODAMB
 */
public class JuegoController implements JuegoDAO {
    
        private final String SELECT_ALL = "select * from cliente";

private Connection con;
public JuegoController(Connection con){
this.con = con;
}

    @Override
    public List<Juego> SelectAllJuego() {
         PreparedStatement stm;
ResultSet rs = null;
List <Juego> lista = new ArrayList<Juego>();
    try{
      stm = this.con.prepareStatement(SELECT_ALL);
          rs = stm.executeQuery();
   while (rs.next()) {

	int codigo= rs.getInt("codigo_juego");
	String nombre = rs.getString("nombre_juego");
	String creador = rs.getString("creador");
	Double precio= rs.getDouble("precio");
        String fechaa=rs.getString("fecha");

	Juego c=new Juego(codigo,nombre,creador,precio,fechaa);

	lista.add(c);
   }
stm.close();
rs.close();
} catch (SQLException e){
e.printStackTrace();
}
return lista;
    }

    @Override
    public Boolean InsertJuego(int codigo_juego, String nombre_juego, String creador, double precio, String fecha) {
      String sql = "insert into juego ( codigo_juego,nombre_juego,creador ,precio,fecha ) values (?,?,?,?,?)";
      
       try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1,codigo_juego);
			ps.setString(2, nombre_juego);
			ps.setString(3, creador);
			ps.setDouble(4,precio);
                        ps.setString(5,fecha);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el juego");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public boolean deleteJuego(int codigo_juego) {
        String sql = "delete from juego where codigo_juego = ?";

try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, codigo_juego);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar el juego");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public Boolean updateJuego(int codigo_juego, String nombre_juego, String creador, double precio, String fecha) {
      String sql = "update juego set  nombre_juego = ?, creador = ?, precio= ?,fecha=? where codigo_juego = ?";//fallo con lo del codigo.
      
      try {
			PreparedStatement ps = con.prepareStatement(sql);

		
			
			ps.setString(1, nombre_juego);
			ps.setString(2, creador);
			ps.setDouble(3,precio);
                        ps.setString(4,fecha);
                        ps.setInt(5,codigo_juego);
			
			
			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al actualizar el juego");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public ResultSet SelectJuegoByName(String nombre_juego) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

    
    
}
