/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34727
 */
public interface EmpleadoDAO {
    public List<Empleado> SelectAllEmpleado();
    public Boolean InsertEmpleado(int codigo_empleado,String nombre_empleado, String apellido_empleado,String ciudad);
    public boolean deleteEmpleado(int codigo_empleado);
    public Boolean updateEmpleado(int codigo_empleado,String nombre_empleado, String apellido_empleado,String ciudad);
     public Boolean SelectEmpleadoByName(String nombre_empleado);
}
