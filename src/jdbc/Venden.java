/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Date;

/**
 *
 * @author 34727
 */
public class Venden {
    private String fecha_venta;
    private int codigo_juego;
    private int codigo_empleado;
    private String dni_cliente;

    public Venden(String fecha_venta, int codigo_juego, int codigo_empleado, String dni_cliente) {
        this.fecha_venta = fecha_venta;
        this.codigo_juego = codigo_juego;
        this.codigo_empleado = codigo_empleado;
        this.dni_cliente = dni_cliente;
    }

    public String getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(String fecha_venta) {
        this.fecha_venta = fecha_venta;
    }

    public int getCodigo_juego() {
        return codigo_juego;
    }

    public void setCodigo_juego(int codigo_juego) {
        this.codigo_juego = codigo_juego;
    }

    public int getCodigo_empleado() {
        return codigo_empleado;
    }

    public void setCodigo_empleado(int codigo_empleado) {
        this.codigo_empleado = codigo_empleado;
    }

    public String getDni_cliente() {
        return dni_cliente;
    }

    public void setDni_cliente(String dni_cliente) {
        this.dni_cliente = dni_cliente;
    }
    
    
}
