/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.Cliente;
import jdbc.Empleado;
import jdbc.EmpleadoDAO;
import jdbc.Juego;

/**
 *
 * @author SEGUNDODAMB
 */
public class EmpleadoController implements EmpleadoDAO {
    private final String SELECT_ALL = "select * from cliente";

private Connection con;
public EmpleadoController(Connection con){
this.con = con;
}

    @Override

public List<Empleado> SelectAllEmpleado() {
       PreparedStatement stm;
ResultSet rs = null;
List <Empleado> lista = new ArrayList<Empleado>();
    try{
      stm = this.con.prepareStatement(SELECT_ALL);
          rs = stm.executeQuery();
   while (rs.next()) {

	int dni = rs.getInt("codigo_empleado");
	String nombre = rs.getString("nombre_empleado");
	String apellido = rs.getString("apellido_empleado");
	String ciudad = rs.getString("ciudad");

	Empleado c=new Empleado(dni, nombre, apellido, ciudad);

	lista.add(c);
   }
stm.close();
rs.close();
} catch (SQLException e){
e.printStackTrace();
}
return lista;
    }

    @Override
    public Boolean InsertEmpleado(int codigo_empleado, String nombre_empleado, String apellido_empleado, String ciudad) {
      String sql = "insert into empleado ( codigo_empleado,nombre_empleado,apellido_empleado ,ciudad ) values (?,?,?,?)";
      
       try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1,codigo_empleado);
			ps.setString(2, nombre_empleado);
			ps.setString(3, apellido_empleado);
			ps.setString(4,ciudad);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el empleado");
			e.printStackTrace();
			return false;
		}

		return true;
    }

    @Override
    public boolean deleteEmpleado(int codigo_empleado) {
String sql = "delete from empleado where codigo_empleado = ?";

try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, codigo_empleado);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar el empleado");
			e.printStackTrace();
			return false;
		}

		return true;

    }

    @Override
    public Boolean updateEmpleado(int codigo_empleado, String nombre_empleado, String apellido_empleado, String ciudad) {
        String sql = "update empleado set  nombre_empleado = ?, apellido_empleado = ?, ciudad= ? where codigo_empleado = ?";
      
      try {
			PreparedStatement ps = con.prepareStatement(sql);

		
			ps.setString(1, nombre_empleado);
			ps.setString(2, apellido_empleado);
                        ps.setString(3, ciudad);
                        ps.setInt(4, codigo_empleado);
			
			
			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al actualizar el empleado");
			e.printStackTrace();
			return false;
		}

		return true;

    }

    @Override
    public Boolean SelectEmpleadoByName(String nombre_empleado) {
        throw new UnsupportedOperationException("Not supported yet."); //No hace falta...........
    }

 
}
